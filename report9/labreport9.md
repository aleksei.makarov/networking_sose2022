
# Practical course Networking Lab 

## Lab9: SNMP - Worked on by Aleksei Makarov and Egi Brako

### Exercise 2

| File | Numerical OID | Full textual descriptor | Children in MIB hierarchy | Data type |
|---|---|---|---|---|
| IF-MIB | 1.3.6.1.2.1.31 | "The MIB module to describe generic objects for network interface sub-layers.  <br> This MIB is an updated version of MIB-II's ifTable, <br> and incorporates the extensions defined in RFC 1229." |  | This data type is used to model an administratively <br>assigned name of the owner of a resource. |
| INET-ADDRESS-MIB | 1.3.6.1.2.1.76 | "This MIB module defines textual conventions for<br> representing Internet addresses.  An Internet<br> address can be an IPv4 address, an IPv6 address,<br> or a DNS domain name.  This module also defines<br> textual conventions for Internet port numbers,<br> autonomous system numbers, and the length of an<br> Internet address prefix." |  |  |
| IP-FORWARD-MIB | 1.3.6.1.2.1.4.24 | The MIB module for the management of CIDR multipath IP Routes. |  |  |
| IP-MIB | 1.3.6.1.2.1.48 | "The MIB module for managing IP and ICMP implementations, but<br> excluding their management of IP routes." |  | This data type is used to model IPv6 address <br>interface identifiers. This is a binary string of <br>up to 8 octets in network byte-order. |
| RFC1155-SMI | 1.3.6.1 |  |  |  |
| RFC1213-MIB | 1.3.6.1.2.1 |  |  |  |
| TCP-MIB | 1.3.6.1.2.1.49 | The MIB module for managing TCP implementations. |  |  |
| UDP-MIB | 1.3.6.1.2.1.50 | The MIB module for managing UDP implementations. |  |  |

See the file `ex2_step5`.

c. The following routing protocols are defined: netmgmt(3), icmp(4), egp(5), ggp(6), hello(7), rip(8), isIs(9), esIs(10), ciscoIgrp(11), bbnSpfIgp(12), ospf(13), bgp(14), idpr(15), ciscoEigrp(16), dvmrp(17), rpl(18).

d. THe following TCP-states are defined: closed(1), listen(2), synSent(3), synReceived(4), established(5), finWait1(6), finWait2(7), closeWait(8), lastAck(9), closing(10), timeWait(11), deleteTCB(12).

e.
### Exercise 3

See the file `ex3_pc2`.

## Exercise 4

See the file `ex4_pc1_wireshark.pcapng`.

1. The header of an SNMP consists of the following fields: the SNMP version, the SNMP community string (it was public in this task) and type of an SNMP message (get-request, get-repsonse, get-next-request).

2. The *snmpwalk* command is designed to perform a sequence of get-next requests automatically, instead of doing the get-next requests by hand.

3. The maximum size of an SNMP packet is 1500 bytes.

4.

## Exercise 5

See the file `ex5_pc1_wireshark.pcapng`.

In this exercise two hosts communicating are 10.0.1.11 and 10.0.1.1, in the previous exercise the hosts were 10.0.1.11 and 10.0.4.14. 

### Exercise 6

a) The output of the command is "Timeout: No Response from 10.0.4.14"
b) The SNMP agent simply lets the command timeout when the community string is incorrect
c) See file `ex6_step_5-8_output`
### Exercise 7
a) The 12 simply stands for a value that we give to the object. The connection gets closed when you change the variable i to 12.
b) As seen during the exercise, an adversary could sniff network traffic to determine the community string. This compromise could enable a man-in-the-middle or replay attack.
### Exercise 8
a) In this case, SNMP v1 and v2c messages have no real observed difference between each other.
b)
c) None, as the credentials are incorrect
d) See files `ex8_step3+4` and `ex8_wireshark_pc1.pcapng`
e) As stated higher, SNMP v1 and v2c have the same `get`, `getnext`, and `set` messages. However, v2c's key advantage over v1 is the Inform command. Unlike Traps, which are simply received by a manager, Informs are positively acknowledged with a response message. In the noAuthNoPriv level, SNMP v3 has no encryption, and it is only authenticated with a username. In the authNoPriv level, it still has no encryption, it contains message digests (in this case using the MD5 algorithm). Then finally, on the authPriv level it has an encryption algorithm (DES), as well as the same message digest algorithm as the level above.
f) The problem with SNMP v3 and especially the authPriv level is not itself specifically, but the underlying algorithms. Namely, DES used for encryption of the messages is extremely vulnerable to Brute-Force attacks.
### Exercise 9
a) See files `ex9_pc1_snmptrapd`, and `ex9_wireshark_pc1`.
b) We got two trap types, `coldStart`, and `enterpriseSpecific`
c) Only 1 message was sent per event
d) See file `ex9_pc1_step9+10`
e) This time, we got 3 different types, `enterpriseSpecific`, `linkDown`, and `linnkUp`.
f) For the link being disconnected and reconnected again, we only got 1 message. For the `enterpriseSpecific` we got multiple
### Exercise 10
a) For every router we can see where the next hop is, determined from the routing table. Then we can follow the next-hop taken from this routing table and see its next-hop. This way we can see where the hypothetical "packet" would go.
b) Before the disconnection of the cable: 10.0.4.14 -> 10.0.4.3 -> 10.0.6.1 -> 10.0.1.1 -> 10.0.1.12
After the disconnecting of the cable: 10.0.4.14 -> 10.0.4.3 -> 10.0.3.2 -> 10.0.2.1 -> 10.0.1.1 -> 10.0.1.12
