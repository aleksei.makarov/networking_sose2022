
# Practical course Networking Lab 

## Lab10: Multicast - Worked on by Aleksei Makarov and Egi Brako

### Exercise 1

See the file `ex1_pc1.pcapng`.

a.

1. The destination MAC-address is 01:00:5e:00:0a:01 (IPv4mcast_0a:01). 

The multicast/broadcast bit is set to 1, see the following line:

```
.... ...1 .... .... .... .... = IG bit: Group address (multicast/broadcast)
```
The globally unique bit is not set, see the following line:
```
.... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
```
2. Yes, the multicast group can be determined from the NIC-specific part of the MAC address, the 2 last bytes in the destination MAC-address are equal to the same in the name of multicast group: *0a:01*.

b. The TTL = 1.

c. We do not observe `IGMP` messages.

### Exercise 2

a. We've reveived 4 `IGMPv3` messages before the first mark, 2 for each connected receiver.

b. The receivers did not react on the new sender, there were no wireshark messages from them to PC1.

c. After the connection of new sender, PC1 did not receive any `IGMP` messages, nevertheless from the packet 100 we can observe that PC4 is also sending his messages to the multicast adddress.

d. To join a multicast group, a host sends a join message using IGMP to its local multicast router router. Then the local router forwward multicast datagrams to hosts in the multicast group.

e. TCP establishes a connection between the sender and the receiver, so TCP is a point-to-point protocol, what is indeed a contradiction to the idea of multicast.

### Exercise 4

See all the files with prefix `ex4`.

a. Socket UDP4-RECV:3333 on PC1 is receiving data from multicast group 224.0.10.3, so from PC2 and PC3. Socket UDP4-RECV:1111 on PC4 is receiving data from multicast group 224.0.10.1, so from PC2 and PC1. Socket UDP4-RECV:2222 on PC4 is receiving data from multicast group 224.0.10.2, so from PC1 and PC3. Socket UDP4-RECV:3333 on PC4 is receiving data from multicast group 224.0.10.3, so from PC2 and PC3.

b. See the file `ex4_pc1_wireshark.pcapng`. The typical TTL value for `IGMPv2`-messages is 1. The destination IP addresses differ, for example 224.0.0.1 is the ussual multicast address, then we can observe many *Membership report* from PC1 and PC4 to the multicast groups, they're contained in. I would assume that the typical destination address of each PC is the address of a multicast group, this PC is belonging to.

### Exercise 5

See the file `ex5_pc4_pings`.

a. All nodes sent an ICMP Echo reply.

b. Yes, we could observe multiple replies from hosts, see the file and you will find DUP packets.

### Exercise 6

See al the files with prefix `ex6`.

a. For each *Membership Query, general* packet we get 2 *Membership Report* packets, one from 10.0.1.1 - interface FastEthernet0/0 of router 1, and 1 from eth0 interface of PC1.

b. The time interval between IGMP *Membership Query* is 60 seconds, there is no fixed time interval between *Membership Report* messages. From the wireshark capture files, we observe that PC1 and PC4 are sending these messages.

### Exercise 8

a. At first, the router 1 was the IGMP querier and router 2 was the IGMP forwarder.

b. After we've changed the IP address FastEthernet0/0 of router 1 we could observe from the wireshark capture, that router 2 became the IGMP querier.

### Exercise 9
a) The designated routers, as shown by files `ex9_5_router1`to `ex9_5_router4` are: Router 1, Router 2, and Router 4 on FastEthernet0/0, whereas on interface FasthEthernet0/1 Router 3 is the designated router.
The querying routers in FastEthernet0/0 are: Router 1 and Router 3, wheras on FastEthernet0/1 the querying routers are: Router 1, Router 2.
b) Multicast group joined by the systems: 224.0.1.40

### Exercise 10
a) We see 3 different message types: Hello, Assert, Join/Prune.
b) Yes, we observe a general membership query as well as a membership report for the group 224.0.1.40, with the source node being the IP address 10.0.1.1
c) There were no unsent PIM messages before the sender was established
d) Since the sender is at Router 1, there are multiple Prune messages that get forwarded to routers "higher up" in the hierarchy shown in the network setup, for example, when Router 3 sends a prune message to router 1.
f) Yes, the UDP packets get sent to the multicast group members.
g)
h) The links don't take that long to reconnect, except for the listener on pc4, which took about a minute.
i)
### Exercise 11
a) All multicast distribution trees are going to be from PIM-DM, which means that all the sources are going to get their multicast traffic from all of the other members of the group, since we're using PIM-DM.
b) See file `ex11_router1`. The (S,G) and (*,G) state entries from the flags. (S,G) means that the group of receivers will receive the traffic from the specified source (Shortest Path Tree). (\*,G) means that the group of receivers will receive the traffic from any/unknown source (Shared Tree). The Flags: 
- S means that the entry is operating in sparse mode. 
- J means that it is part of the shortest path tree. 
- C means that a member of the multicast group is present on the directly connected interface. 
- F means that the software is registering for a multicast source. 
- T indicates that packets have been received on the shortest path source tree.
- P means that this route has been pruned.
- L means that the router itself is a member of the multicast group.

c)

### Exercise 12
a) See files `ex12_mrinfo_router1` and `ex12_mtrace_router4`
b) mtrace is useful to see which path the multicast packet takes to go to the destination, which you cannot see from any of the commands in part 3

### Exercise 13
a) We can observe a new PIM message, called RP-Reachable.

``` 
Protocol Independent Multicast
    Type: PIM (0x14)
    Code: RP-Reachable (4)
    Checksum: 0xecc2 [correct]
    [Checksum Status: Good]
    0001 .... = Version: 1
    Reserved byte(s): 000000
    PIM Options
        Group Address: 224.0.1.40
        Mask: 255.255.255.255
        RP: 10.0.3.2
        Holdtime: 270
    Reserved byte(s): 0000
```
 
b) See the file `ex13_part9_router2`
c)

### Exercise 14
a) By default, the router switches to an SPT after receiving the first packet on the shared tree for a given (S,G) address pair. When the first data packet arrives at the redezvous point from Router 1, it creates a multicast table entry for the address pair (10.0.1.2, 224.0.10.1). This packet is a unicast packet that is encapsulated in a PIM register message. The RP then sends a Join/Prune message to Router 1, since it is on the reverse path towards the IP of PC1. When Router 2 receives a multicast message from PC1, it knows that the SPT is at least partially created from PC1 to itself. Then it sends a register-stop message to Router 1, to tell the router to stop sending packets in PIM register messages. After this, upon receiving a multicast packet from PC1, all routers will switch to the SPT. They will send Join/Prune messages to change the route.
