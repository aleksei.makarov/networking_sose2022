# Practical course Networking Lab 

## Lab1 - Worked on by Aleksei Makarov and Egi Brako

### Exercise 1

a) We logged in as user with the following data: login - **ubuntu**, password - **lab**.

b) To setup the networks we used Ethernet cables, plugging them into corresponding interfaces *eth0* on PC's.

c) We used the commands `(PC1) $ telnet 10.0.1.12` to start telnet session from *PC1* to *PC2*.

### Exercise 3

In this exercise we used 3 methods: two via `>`, specifically `ls -l > etcfile_1_ex3` and `ls > etcfile_2_ex3 & tail -f etcfile_2_ex3`. The `tail` command is used to output the end of the file. The `-f`-option is used to output appended data as the file grows. (Taken from `man` page). The 3<sup>rd</sup> is just use of `gedit`. 

See the files `etcfile_1_ex3`, `etcfile_2_ex3`, and `etcfile_3_ex3`.

### Exercise 4

a) See the file `dffile_1`

b) See the directories `labdataPC2`, `labdataPC3`, and `labdataPC4`.

### Exercise 5

a) The file `/etc/hosts` is contained in the file `ex5`.

b)

1. The file `/etc/hostname` contains the hostname. After changing it, we should also change any occurence of the previous name to the new one in the file `/etc/hosts`. Then we need to reboot the system with `sudo reboot`.

2. To check if the the IPv4-forwarding is enabled check the file `/proc/sys/net/ipv4/ip_forward`. Depending on the value (1/0) we conclude, if the forwarding is enabled or not.

3. The  network interface configuration files can be found at `/etc/network/interfaces`.

### Exercise 6

1. See the files `ping2pc1` and `ping2pc2` for corresponding pings to *PC1* and *PC2*. See the file `ping2loopback` to look at the output of ping from *PC1* to its loopback interface. 

2. The loopback interface is a visual network interface through which network applications can communicate within the same device. It is implemented entirely within the operating system and passes no packets to any network interface controller. In opposite, the local Ethernet interface refers to a circuit card installed in device as a network client. That means, that the `ping 127.0.0.1` (commonly used loopback IP address) will be faster than `ping 10.0.1.11` from *PC1* to himself. This happens because the first signal will be delivered immediately, whereas the second will pass through the switch, then get rerouted to the source. 

### Exercise 7

a) See the files `ping_ex7_1` and `tcpdump_ex7_2`.

<br>

In the first file (`ping_ex7_1`) we can see a few fields in the packets. Firstly is *icmp_seq=1*. This is simply the sequence number that is given to this packet by the Internet Control Message Protocol.

Second is *ttl=64*. This is the remaining time to live of the current packet. It gets decremented at every hop, and once it reaches 0, the packet is dropped.

Third is *time=0.370ms*. This is the total time it took to send and receive the echo packet including delay time. So in other words, the round trip time

<br><br>

In the second file (`tcpdump_ex7_2`) We see more or less the same fields.

*16:20:59.923248* is the exact time of the packet.

*IP* is the network layer protocol, in this case the Internet Protocol.

*10.0.1.11* is the source IP address.

*10.0.1.12* is the destination IP address.

*ICMP echo request* is the type of packet, in this case it's the source requesting the destination for an echo response.

*id 4266* is the identification number of the packet.

*seq 1* is the sequence number of the packet

*length 64* is the size of the packet, in this case 64 bytes.



<br>

b) See the file *tcpdump_ex7_b*. In our case only *PC2* responded to broadcast ping.

### Notes

The times on the packets are wrong as the times on the computers were wrong.




