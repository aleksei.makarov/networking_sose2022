# Practical course Networking Lab 

## Lab4 - Worked on by Aleksei Makarov and Egi Brako

### Exercise 2

* The contents of packets captured with wireshark are contained in `ex2_part7pc1.out` and `ex2_part7pc4.out`.

* The contents of *quagga# show ip rip* are contained in `ex2_showiprippc1, ..., ex2_showiprippc4`.

* The content of *netstat -rn* is contained in `ex2_step4`.

* The content of *traceroute* is contained in `ex2_trace`.

1. The destination is always *224.0.0.9* - a multicast address. This address is used to send routing information to designated routers on a network segment.

2. Yes, each router advertises its full routing table over all of its own RIP-interfaces. And whenever a RIP router receives such an adverntisment it puts all routes into its routing table and uses the updated table to forward packets.

3. We have seen two types of messages: request and response. The request message asks for content of RIP-tables. Since the address (multicast) is not specified, then the metric will have the value 16 (the maximal number of hops for RIP is 15, the metric is number of hops). The response messages are always coming from the corresponding interfaces of routers and they advertise the contents of its RIP-table to PC.

4. Request message has length of 66 bytes, response - 106, the content is RIP-table of router.

### Exercise 3

a) For part a you can take a look in files `ex3a_pc1, ..., ex3a_pc4` and `ex3a_pc2after, ex3a_pc4after` - to see the content of routing tables after update of network topology.

b) The output of *ping* command is contained in the file `ex3b_pingstatistics`. The overall time of ping command is circa 217 seconds, the number of lost packets is 151, the amount of all packets is 218. So we conclude that the necessary time to update the routing table is 151 seconds.

### Exercise 4

b) To see the debugging information on router 3 see the file `ex4b_5_router3` and `ex4b_rippc3.out` for wireshark output. In wireshark packets we can see the count-to-infinity problem appearing when the metric value is 16. 

c) Unfortunately we could not see any differences in convergence time of routing tables, therefore we did not include any output file. The times were the same.

### Exercise 5

Unfortunately the wireshark file is corrupted, so we could not solve the questions where wireshark output was needed. But in the file `ex5c_ping` there is output of ping command, we could observe that the convergence time was much less then for RIP, actually this took only 6 seconds. The ospf databases of pc2 and pc3 are contained in the files `ex5c_ospfdbpc2, ex5c_ospfdbpc3`. The ospf databses of routets and pc's are the same (only ages of other hosts are different).

### Exercise 6

4. The outputs of `show ip ospf database` for any pc are saved in files `ex6_4_pc1, ..., ex6_4_pc4`. The numbers of pc's are unfortunately not like in the lab, but as in book, the subnets are also sometimes different, because there was an error of overlapping with serial ip-address.

5. Here we need to explain what is the difference between router link states, net link states and summary link states. Router link states are all routers and net link states are multi-access networks. The summary network link states give information about every network segment in the network outside of the area.

6. The routers in one area don't know anything about topology of other areas, they know only ip-addresses through which the information will be retransmitted from/to other areas.

7. It seems that there are no routers in area 0, if we are talking about pc's (pc3 and pc4 in our case) then they also don't know much about the topology of other area. They just now the interfaces, which will be used to transmit the packets from/to ip-addresses in other areas.

We observe in this exercise many OSPF Hello-packets.

### Exercise 7

a) All the necessary files afor this part are `ex7a_wireshark_full.out` and `report4_ex7_a_routers`. For wireshark capture we can see different types of messages: OPEN, KEEPALIVE and UPDATE message. The OPEN messages are used to establish BGP connection. The UPDATE message is used to exchange routing information between routers, also the *AS_PATH* information is contained in UPDATE messages. KEEPALIVE messsages are exchanged every 1/3 of the Hold Time period between the routers. If the Hold Time is equal 0, then no KEEPALIVE messages will be sent between BGP neighbours.

ASes aanounce their routing policy to other ASes and routers via BGP. BGP's inter-AS configuration allows two ASes to communicate between each other. BGP supports next-hop, so BGP is able to find the best possibility for next-hop. In the BGP advertisment is contained the path information about next-hop on the destination and which destinations are reachable. BGP runs over TCP, TCP makes sure that data packets get retransmitted across networks.

b) See the files `ex7_b_wireshark_full.out, report4_ex7_b_neigh23, report4_ex7_b_router2`. Here we can see a new type of BGP message, a NOTIFICATION message, that's exactly the type, which indicates an error condition to a BGP neighbor.

