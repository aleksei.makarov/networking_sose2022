# Practical course Networking Lab 

## Lab6: Worked on by Aleksei Makarov and Egi Brako

### Exercise 1

All the necessary information is included in files with prefix *ex1*, for example `ex1_pc1` describes the outputs of *ping* and *traceroute* commands. 

1. The bridge does not manipulate any of the fields in the MAC and IP headers. You can observe this by looking through the file `ex1_pc1_wireshark1.pcapng`. Take a look to the corresponding headers on each frame.

2. The bridge does not manipulate any of the fields in the MAC and IP headers. You can observe this by looking on the corresponding headers in `ex1_pc3_wireshark1.pcapng`. See any frame. If PC2 was configured as a router, then PC2 would substitute the MAC address of destination host with its own one. The source and destination IP addresses would remain the same.

3. The output of *traceroute* command is contained in the file `ex1_pc1`. PC2 does not appear in the output of *traceroute* command because it is configured as a bridge. If PC2 would be configured as a router, then one of its interfaces (probably *eth1*) would appear in the *traceroute* table.

4. If it is necessary you could take a look at additional files.


### Exercise 2

1. The results are not different from the first exercise, see the file `ex2_pc1`.

2. We cannot ping the router, because we did not configure the IP address of router. 


### Exercise 4

a)

1. Take a look at files `ex4a_pc1`, `ex4a_pc3`, `ex4a_pc4`.

2. We can observe the learning process of router 1 by executing *show bridge* command multiple times. After the first *ping* command the router 1 will learn the MAC address of router 2 and the address of eth0 interface of PC2.

3. The first ping command was issued from PC1 to PC2, therefore router 1 does not know the MAC addresses of PC3 and router 2. After the third *ping* command the router 2 will gain the information about MAC addresses of router 3 and PC4. After the third command, router 1 will learn about interfaces *eth0* of PC3 and PC4.

b)

1. Take a look at the file `ex4b_pc1`, it took approximately 18 seconds until the *ping* commands became succesfull again.

### Exercise 7

b)

1. Let's take a look at the following command: *ping -c 1 10.0.4.31*, to observe the path taken by pakets, you can check the files `ex7b_pc1`, `ex7b_pc3`, `ex7b_pc4`. The source is RealtekS_00:0a:10 (00:e0:4c:00:0a:10) and destination - Cisco_6b:9d:61 (00:21:55:6b:9d:61), it's address of the inerface FastEthernet0/1 of router 2. From the file `ex7b_pc3` we can observe that the source is Cisco_9c:0c:e1 (00:1f:ca:9c:0c:e1)and destination - RealtekS_00:0a:30 (00:e0:4c:00:0a:30), that's an interface FastEthernet0/0 of router 2.

Now we get the following path PC1 $\to$ router 2 (FastEthernet0/1) $\to$ router 2 (FastEthernet0/0) $\to$ PC3

*ping -c 1 10.0.4.41* gives the following path: PC1 $\to$ router 2 (FastEthernet0/1) $\to$ router 2 (FastEthernet0/0) $\to$ PC4

*ping -c 1 10.0.3.21* gives the following path: PC1 $\to$ router 2 (FastEthernet0/1) $\to$ PC2.



