# Practical course Networking Lab 

## Lab5: UDP and TCP - Worked on by Aleksei Makarov and Egi Brako

### Exercise 2

See the files `ex2_pc1_wireshark.pcapng`, `ex2_pc2_wireshark.pcapng` and `ex2_pc1.pcapng` for the same retransmission but with different source port. 

1. There were 14 UDP-packets totally exchanged. There were 10 UDP-datagrams, eaxh of them carrying 1024 bytes of data. So for every UDP-datagram we had one packet. 

2. The size of UDP payload for each datagram is 1024 bytes.

3. The size of application data is $1024 * 10 = 10240$ bytes. The total number of bytes including headers (42 bytes) is $1066 * 10 + 46 *4 = 10844$, so the ratio is $0.94$.

4. In UDP headers the following fields were the same:
10.0.5.11 10.0.5.22 UDP 1066 45049 → 5038 Len=1024: IP-addresses, length source and destination ports, length of UDP-packet.

5. If we try to repeat the same exercise then the port on PC1 will be other, it will be 39030, but the destination port will be the same: 5038 but not 4444 as we have chosen, what was strange.

### Exercise 3

See the file `ex3_pc1_wireshark.pcapng`.

1. There were 38 TCP-packets totally exchanged. 20 of them sent by PC1, 18 by PC2. There was 1 packet sent for each TCP segment. The other 28 packets do not carry payload.

2. There are some different sizes of TCP-segments: 66, 74, 81, 145, 151, 1090 bytes.

3. The size of application data is $1024 * 10 = 10240$ bytes. The total number of bytes of all TCP-packets is $12959$ bytes, the ration is then $0.79$, so the ratio is less than in the previous exercise.

4. All of the TCP headers contain different flags. We observed packets with [SYN], [SYN, ACK], [ACK], [FIN], [FIN, ACK], [PSH, ACK] flags or pairs of them. SYN is used for synchronization, to initialte a connection, ACK - acknowledgment, used to confirm the data transmission, FIN - finish, used to close the connection, PSH - used to indicate that the data should be passed on directly to the application instead of buffering.

### Exercise 4

See the files `ex4a_pc1.pcapng`, `ex4b_pc.pcapng` and `ex4_pc2` for output of the commands.

1. For *ftp* it took $0.89$ seconds, for *tftp* - $4.1$ seconds.

2. First of all we could observe *ftp* connections to establish the connection and then *ftp-data* connections each of them to transmit *1448* bytes of data.

### Exercise 5

See the files `ex5_pc1.pcapng`, `ex5_pc2.pcapng` and `ex5pc1_lchange`.

1. The threshold could be found by trying different values of -l parameter. This is $65507$ bytes. 

3. See the captured wireshark files.

4. The different header fields are *UDP* and *IPv4*, the packets with *IPv4* headers are just data packets, which will be reassembled in the next UDP packet.

### Exercise 6

See the files `ex6_1_pc1`, `ex6_1_pc3` and `ex6_2_pc1`.

Case 1:

1. Here we don't observe any fragmentation, because the MTU's on all interfaces of PC1 and PC3 are 1500 bytes.

2. Since there are no fragmentations on PC1 and PC3, there are also no *ICMP* error messages.

3. DF flag is don't fragment flag, here this flag is set.

Case 2:

1. There are fragmentations, because the MTU on the interface *eth1* is 500, since PC3 retransmits all packets from PC1 to PC2, then we can obsereve retransmissions.

2. There are *ICMP* error messages, they are used to show that fragmentation is needed. 

3. See the wireshark captures.

### Exercise 7

1. 

1. The handshake packets are [SYN], [SYN, ACK], [FIN, ACK], [ACK]. In each of them is set the flag *Don't fragment*.

2. [SYN], [SYN, ACK] - used to  ask for the establishment of connection and its acknowledgement, [FIN, ACK] - used to ask for the termination of connection, then the client sends [ACK] to acknowledge it.

3. The initial sequence numbers are both 0 for client and server, the absolute sequence numbers are 2154467699 and 28651819 respectively.

4. The relative sequence number for TCP Data segment is 1.

5. The initial window sizes are 29200, 28960 for client and server respectively.

6. The MSS value for TCP client and TCP server is the same, namely 1460 bytes.

7. The time for establishment of TCP connection is the time necessary to execute a three way handshake, so 0.000372 seconds.

8. The packets used for connection termination are [FIN, ACK] and [ACK]. The client sends [FIN, ACK] packet to terminate the connection and the server sends [FIN, ACK] to acknowledge this request, finally the client sends [ACK] as a response to terminate the connection.

2. 

1. Here we have multiple Telnet Data packets sent sequentially and in the first case we have TCP retransmission packets, because we had to close telnet session manually.

2. The timeout time is 60 seconds.

### Exercise 8

1. The pattern is the following: the clients doubles the time interval after which he tries to reconnect to the host.

2. The first packet tries to establish the connection, the next packets are TCP retransmission packets, in the TCP analysis flags is written that these are suspected retransmissions according to SEQ/ACK analysis, but they have SYN flags, so it means that TCP client tries to reset the connection.

3. 

### Exercise 9

See the file `ex9_pc1.pcapng` for wireshark capture.

The host closed the connection by resetting it. To end this we needed the time in interval from the last [SYN] packet sent until the [RST, ACK] packet was received. This took 0.000117 seconds.

## Advanced TCP

### Exercise 2

1. 

1. There are 3 packets for each character, 2 of them with data from source to destination host and vice versa and 1 for an acknowledgement.

2. This could be explained as follows on example of packets 60 - 62: We observe that the sequence number of packet 61 is the same as ACK of 60, the same for packet 62 and all other packets. Packets 60, 61 have the flag [PSH, ACK] and 62 - [ACK].

60: Transmission Control Protocol, Src Port: 38546 (38546), Dst Port: 23 (23), Seq: 116, Ack: 257, Len: 1

61: Transmission Control Protocol, Src Port: 23 (23), Dst Port: 38546 (38546), Seq: 257, Ack: 117, Len: 1

62: Transmission Control Protocol, Src Port: 38546 (38546), Dst Port: 23 (23), Seq: 117, Ack: 258, Len: 0

3. With delayed acknowledgment the host can reduce the number of packets, the client waits if there are any other packets which could be sent to the same direction as a pack. The delay of ACK for a character is the time difference between the 3.rd and 2.nd packet, in upper case that is 0.000024 seconds.

4. The time delay associated with the transmission of ACK from the telnet server is the difference between the 2.nd and 1.st packet, in upper case that is 0.000179 seconds.

5. For every character both 1.st and 2.nd packets carry a payload, they have the flag [PSH, ACK], also they carry acknowledgment for previous packets. The 3.rd packet has [ACK] flag, so only acknowledgment for previous packet.

6. Let's see the packet 63:

63: Transmission Control Protocol, Src Port: 38546 (38546), Dst Port: 23 (23), Seq: 117, Ack: 258, Len: 1

We observe that the packet 62 did not consume any sequence number, since the packets 62 and 63 have the same seq. number. The packet is necessary to verify which packets should be acknowledged. But since there are no gaps in sequence numbers, then there will be no confusion at TCP receiver.

7. As we type with the ususal speed the window size did not change, it's value was 29312 bytes but if we hold for example any button for some time the window will be increased to 30336 bytes.

2. 

2. If we increase the typing speed (or just hold some button) then the TCP payload can increase, for example 6 bytes in frame 334. We will also get only one ACK packet for multiple characters transmitted.

3. From the upper example we can find out the time 0.27 seconds.

### Exercise 3

1. There are 3 packets for each character, 2 of them with data from source to destination host and vice versa and 1 for an acknowledgement.

2. Like in the previous exercise, as we type too quickly, we can observe one segment carrying multiple characters.

3. We observe also delayed acknowledgments if we type quickly. Similar to the previous exercise the acknowledgment will be delayed, awaiting data which will be also retransmit in the same direction.

4. Yes we were able to observe the transmission of multiple characters in the same packet. For example take a look at packets 90-106 in the corresponding wireshark capture.

### Exercise 4

1. For the first 190 the TCP receiver sent 1 acknowledgment for each received TCP packet, but for the next packets the TCP receiver has sent 1 acknowledgment for 2 recieved TCP packets.

2. The first 10 times there were 1000 bytes acked, 180 times - 1448 bytes and ~ 379 times - 2896 bytes, so in average there were 2404 bytes acked at once.

3. We suppose that this were 2896 bytes.

4. The window size changed from 28960 at the establishing of TCP connection to 237568 in the final packet with [FIN, ACK] flag.

5. We can take for example packet 367, it's acknowledgment to the packet 360, the time from transmitting it from PC1 to erceive an acknowledgment was 0.00055 seconds.

6. TCP did not use the full size of congestion window. The maximal amount of data which was sent in 1 TCP segment was 1448 bytes, this is less then allowed in a window size, see the size of a window without scaling. We suppose that this happens because of congestion control of TCP protocol.

7. After the sender closes the connection, the TCP receiver continues to send acknowledgments for the previous packets, he's received.

### Exercise 5

1. The pattern changed, so for example we began to observe 2 acknowledgments from receiver for 1 retransmitted segment, we can observe it periodically from the packet 85. Here we observe new additional packets, for example TCP Retransmission, TCP Dup ACK packets. We get also in addition TCP Window Update.

2. The frequency is now different, like mentioned in the first part, 2 acknowledgments from receiver for 1 transmitted segment, TCP Dup ACK's and also 1 acknowledgement packet for 1 transmitted packet.

3. The window size from for packets from PC1 to PC2 is constatly growing by amount of transmitted data. The windows size for packets from PC2 to PC1 has the similar pattern for changing the window size.

4. No, this could happen because of congestion window control.

### Exercise 6

1. The *SACK Permitted* option is included in the handshake, take a look at options TCP panel of packets with [SYN] flag.

2. 

1. After the first disconnect the TCP Retransmission packets occure periodically between TCP DUP Acknowldgment packets. We also observed TCP Retransmissions one after another in the packet interval 510-515.

2. We could observe the big time interval between TCP Retransmissions for the packets 508 and 507, the time between this packets is 126 seconds.

3. There were different patterns, ususally it would be only one TCP Retransmission for a TCP segment. But for example the packets 506-508 have the same sequence number, because the ACK for the packet with sequence number failed.

3. 

1. Here we observe more TCP retransmission packets and less TCP Dup ACK.

2. We could observe TCP Fast Retransmission, see the packet 1936 and also packets with SACK, for example packet 1914.

### Exercise 7

1. According to the graph, see the file `graph.pdf`, we suppose that first threshold value is 365, because after this packet the growth of RTT will be linear.

2. I'm not sure about where the slow start happens (actually this should be exponential growth of congestion window after the drop of TCP transmission), but we suppose this could happen at time period from ~ 22 to ~ 29 seconds.

3. The fast recovery happens after every drop of RTT to 0, the linear growth immediately after drop is exactly the fast retransmit.
