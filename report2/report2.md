# Practical course Networking Lab 

## Lab2 - Worked on by Aleksei Makarov and Egi Brako

## Here is an important notice. For exercises from 1 to 9 we were supposed to change the IP addresses of PC's from default to the special, mentioned in the network setup. We unfortunately did not do this, but it did not affect the sense and idea of the exercises. It was important only for exercise 7, but by then we had noticed it and changed it respectively for that one.

### Exercise 1

See the files `ex1, ex1_2`, the second file contains only *ICMP*-messages (the same for removing all non-*ICMP*)

### Exercise 2

See the file `ex2.out`.

### Exercise 3

See the file `ex3.out`.

### Exercise 4

1. See the file `ex4_1.out`.

2. See the file `ex4_2.out`.

3. See the file `ex4_3.out`.

### Exercise 5

See the file `exs5.out`.

1. The typical MAC address of an ARP request packet should be the Ethernet broadcast address: *ff:ff:ff:ff:ff:ff*.

2. The type field in received Ethernet headers is `Ethernet II`.

3. The process which ARP uses to acquire a MAC address for a given IP address works so:


We want to send an `ICMP` Echo request from PC1 to PC2. We do this by usual `ping` command.

If the MAC-address in the cached ARP table on PC1 is not outdated yet, then PC1 just sends the Ethernet frame containing the IP packet. Otherwise PC1 should send a broadcast ARP request message (dest. MAC address will be Ethernet broadcast address).

Then PC2 responds with an ARP response message containing its MAC address.

On PC1 the cached ARP table will be updated, then PC1 can send the Ethernet frame with IP packet.




### Exercise 6

See the file `ex6`.


### Exercise 7

See the file `ex7.out`.

1. The interval between each ARP request is nearly 1 second. Since PC1 is not receiving any ARP response message, it continues to send ARP requests to a Ethernet broadcast address.

2. ARP cannot be encapsulated, because it's exactly the job of ARP itself: encapsluta IP datagrams into Ethernet frames, they are just working on different network layers.

### Exercise 8

1. See the files `ex8_1, ex8_2`. 
2. The mtu for a loopback interface is 65535 bytes, for *eth0* - 1500 bytes.

### Exercise 9

See the files `ex9_1` and `ex9_2`.

### Exercise 10

See the file `ex10_1` for output.

To explain the output of the command `ip addr` let's take a look at the following line:

<br>
3: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:e0:4c:00:0a:10 brd ff:ff:ff:ff:ff:ff
    inet 10.10.10.11/24 brd 10.10.10.255 scope global eth0
    
*eth0* - name of the interface
*BROADCAST, MULTICAST, UP, LOWER_UP* - interface supports broadcast and multicast, is turned on (UP) and has connected a wire into it (LOWER_UP)
*mtu* - maximum transmission unit - 1500 bytes
*qdisc* - is the queueing discipline. *mq* is a dummy scheduler for this.
*group default* - the group of interfaces give a single interface to user by combining the capabilities
*qlen* - length of queue, if there are 1000 packets in queue, the next one will be dropped

The next few lines describe the MAC address (and a broadcast address) and IP address,
<br>

### Exercise 11

See the file `ex11_1` for output.

1. The hostname is PC4.

2. There will be no error message, because the last device, which sent an ARP response simply overwrote the corresponding entry of the ARP table on PC1.

### Exercise 12

The files `ex12_1to2, ex12_1to3, ex12_1to4, ex12_2to1` contain the results of `ping` commands. There is no output file since we've got the mistake that host is unreachable. The file `ex12_1_all_wireshark.out` is the capture of all packets from/to PC1.

### Exercise 13

1. This method is impractical for networks with a lurge number of hosts, because we should save all mappings for each host. Therefore if just one hosts changes his name, we should do it for every other host too.

2. The result will be just the first ip address, which will be found from beginning of the file with the host name we are looking for.

### Exercise 14

See the file `ex14.out` for output.

1. The ftp client uses a random high-numbered port for the source, in our case the Src port was 53565. The ftp server uses destination (Dst) port 21.

2. The login name and pass could be found in a file in following segments: Request: USER ubuntu, Request: PASS lab.

### Exercise 15

See the file `ex15.out` for output. Thre are multiple lines which contain separate characters `u, b, u, n, t, u` and `l, a, b`. The transmission of login and password begins at the line 187: `    Data: PC2 login: `. While the behaviour may be a bit different from the two protocols, as much as it concerns the user they have the exact same security flaws.

### Exercise 16

See the file `ex16.out` for output. The 3 for each character are defined as follows: The first is just source-destination packet, which contains the data. The second is ACK from receiver to source and the third is ACK on ACK, sent from source host to receiver.


