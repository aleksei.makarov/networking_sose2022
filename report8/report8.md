# Practical course Networking Lab 

## Lab8: Debugging a server program (daemon) - Worked on by Aleksei Makarov and Egi Brako

### Exercise 2

If the same name is assigned to multiple different IP addresses, then the `ping` command will be issued only to the first host, for which the given name is assigned.

### Exercise 3

a)

```
$TTL 86400
mylab.com. IN SOA PC4.mylab.com.  hostmaster.mylab.com. (
			1 ; serial
			28800 ; refresh
			7200 ; retry
			604800 ; expire	
			86400 ; ttl
			)
;
mylab.com	IN	NS	PC4.mylab.com.
;
localhost      A	127.0.0.1
PC4.mylab.com. A	10.0.1.41
PC3.mylab.com. A	10.0.1.31
PC2.mylab.com. A	10.0.1.21
PC1.mylab.com. A	10.0.1.11
```
According to the exercise sheet PC4 is set up as DNS server, other PC's are hosts. In the file `db.mylab.com` is defined the DNS zone `mylab.com`.
The first block desribes the time period after which the corresponding action will be performed. The line `$TTL 86400` describes how long the DNS server will be alive, in our case it is 86400 seconds = 24 hours. The line `28800 ; refresh` specifies the interval used by DNS clients to refresh their registration in the zone. 

In the second block the DNS hosts are configured by their names with corresponding IP addresses, like in the network setup.

b) 

c) You can find the necessary output in files `ex3c_pc1, ex3c_pc4`. Now will explain the output of the commands in the file `ex3c_pc4`.
The `host` command is used to find the domain name in the DNS zone configuration file. There are 4 different sections in the output: the `QUESTION SECTION` gives us name or the address of the host, we are looking for. In the `ANSWER SECTION` we get the IP address of the host, if we've requested a lookup for domain name of host and vice versa. In the `AUTHORITY SECTION` we get the name of DNS server. In the `ADDTIONAL SECTION` we get the IP address of the DNS server.

d) To see the necessary output see the files `ex3d_pc1, ex3d_db.lab8.net, ex3d_named.conf`.

e) To see the necessary output see the files `ex3e_pc1, ex3e_db.mylab.com, ex3e_db.10.0.2`.

### Exercise 4
