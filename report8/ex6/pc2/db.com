
$TTL 86400
com.	IN	SOA	PC2.mylab.com.	hostmaster.mylab.com. (
			1 ; serial
			28800 ; refresh
			7200 ; retry
			604800 ; expire	
			86400 ; ttl
			)

;
com.	IN	NS	PC2.mylab.com.
;
PC2.mylab.com.    	IN  A     10.0.1.21
;
mylab.com.		IN  NS    PC4.mylab.com.
PC4.mylab.com.  	IN  A     10.0.1.41
;
Router4.com.	  	IN  A     10.0.1.141
;
top-server.com.	  	IN  A     10.0.1.21
;
