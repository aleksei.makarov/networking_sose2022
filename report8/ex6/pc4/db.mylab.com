
$TTL 86400
mylab.com.	IN	SOA	PC4.mylab.com.	hostmaster.mylab.com. (
			1 ; serial
			28800 ; refresh
			7200 ; retry
			604800 ; expire	
			86400 ; ttl
			)

;
mylab.com.	IN	NS	PC4.mylab.com.
;
;
;
PC1.mylab.com.    	IN  A     10.0.1.11
PC2.mylab.com.    	IN  A     10.0.1.21
PC3.mylab.com.    	IN  A     10.0.1.31
PC4.mylab.com.    	IN  A     10.0.1.41
R1.mylab.com.    	IN  A     10.0.1.111
R2.mylab.com.    	IN  A     10.0.1.121
;
nameserver.mylab.com.  	IN  A     10.0.1.41
;
